// public
import React from "react";
import { Grid, Container } from "@material-ui/core";

// private
import fetchMeetupEvents from "../api/FetchMeetupEvents";
import EventCard from "./EventCard.js";
import {
  convertDescriptionToText,
  convertToReadableDate
} from "../utils/formatResponses";
// import convertToReadableDate from "../utils/formatResponses";

class EventGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      events: []
    };
  }

  componentDidMount() {
    fetchMeetupEvents().then(
      result => {
        this.setState({
          isLoaded: true,
          events: Object.values(result)
        });
      },
      error => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    );
  }

  render() {
    const { error, isLoaded, events } = this.state;
    if (error) {
      return (
        <Container className="container">Error: {error.message}</Container>
      );
    } else if (!isLoaded) {
      return <Container className="container">Loading...</Container>;
    } else {
      return (
        <Grid key="grid" container justify="center" spacing={2}>
          {events.map(event => (
            <Grid key={event.id} item>
              <EventCard
                key={event.id}
                id={event.id}
                name={event.name}
                time={convertToReadableDate(event.time)}
                venue={event.venue.name}
                rsvp_count={event.yes_rsvp_count}
                description={convertDescriptionToText(event.description)}
              ></EventCard>
            </Grid>
          ))}
        </Grid>
      );
    }
  }

  componentWillUnmount() {}
}

export default EventGrid;
