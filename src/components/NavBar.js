// public
import React from "react";
import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// private
import ReactJSDallasSVG from "../images/reactjs-dallas.svg";
import GitLabIcon from "../images/gitlab-logo.svg";

const useStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.background.header,
    flexGrow: 1
  },
  title: {
    justifyContent: "left",
    flexGrow: 1,
    display: "flex"
  },
  logo: {
    display: "inline-block",
    flexShrink: 0,
    width: 30,
    height: 30
  }
}));

export default function NavBar() {
  const classes = useStyles();

  return (
    <AppBar position="fixed">
      <Toolbar className={classes.root}>
        <Button href="/callemall-meetup-app/" className={classes.title}>
          <ReactJSDallasSVG className={classes.logo} />
          <Typography varient="title" color="primary">
            ReactJS Dallas
          </Typography>
        </Button>
        <Button href="/callemall-meetup-app/events" color="secondary">
          Upcoming Events
        </Button>
        <IconButton
          className={classes.logo}
          href="https://gitlab.com/JenTheMaker/callemall-meetup-app"
        >
          <GitLabIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}
