// public
import React from "react";
import {
  Box,
  Button,
  Grid,
  Icon,
  Typography,
  Container
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LocationOnIcon from "@material-ui/icons/LocationOn";

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(10),
    paddingBottom: theme.spacing(3),
    width: "100%",
    minWidth: 360,
    backgroundColor: theme.palette.background.header,
    color: theme.palette.secondary.main
  },
  title: { fontWeight: 600 },
  location: {
    color: "rgba(255, 255, 255, 0.7)",
    justifyContent: "left",
    flexGrow: 1,
    display: "flex"
  },
  time: {
    fontSize: 14,
    color: "rgba(255, 255, 255, 0.7)",
    textTransform: "uppercase"
  }
}));

export default function EventHeader(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container>
        <Grid container direction="column">
          <Grid container direction="row">
            <Grid item xs>
              <Typography component="div">
                <Box m={1} className={classes.time}>
                  {props.time}
                </Box>
                <Box fontSize="h4.fontSize" fontWeight="fontWeightBold" m={1}>
                  {props.name}
                </Box>
              </Typography>
            </Grid>
            <Grid item>
              <Button
                href={"https://www.meetup.com/ReactJSDallas/events/" + props.id}
                color="secondary"
                variant="outlined"
              >
                Meetup Link
              </Button>
            </Grid>
          </Grid>
          <Grid item>
            <Typography className={classes.location} varient="subtitle2">
              <Icon aria-label="LocationOn">
                <LocationOnIcon />
              </Icon>
              {props.venue}
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
