// public
import React from "react";
import { Avatar, Card, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    maxWidth: 100,
    padding: theme.spacing(2),
    backgroundColor: theme.palette.background.card,
    color: theme.palette.secondary.main
  },
  avatar: {
    width: theme.spacing(10),
    height: theme.spacing(10)
  }
}));

export default function EventAvatars(props) {
  const classes = useStyles();

  return (
    <Card className={classes.root} varient="outlined">
      <Avatar className={classes.avatar} alt={props.name} src={props.img} />
      <Typography color="secondary">{props.name}</Typography>
    </Card>
  );
}
