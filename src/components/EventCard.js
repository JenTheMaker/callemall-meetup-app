// public
import React from "react";
import {
  Card,
  CardActionArea,
  Typography,
  Icon,
  CardContent
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LocationOnIcon from "@material-ui/icons/LocationOn";

const useStyles = makeStyles(theme => ({
  root: {
    textAlign: "left",
    minWidth: 275,
    maxWidth: 800,
    background: "#424242",
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(3),
      flexDirection: "row",
      alignItems: "flex-start",
      textAlign: "left"
    }
  },
  title: {
    color: theme.palette.secondary.main
  },
  time: {
    fontSize: 14,
    color: "rgba(255, 255, 255, 0.7)",
    textTransform: "uppercase"
  },
  location: {
    color: "rgba(255, 255, 255, 0.7)",
    justifyContent: "left",
    flexGrow: 1,
    display: "flex"
  },
  description: {
    color: theme.palette.secondary.main,
    maxHeight: "100px",
    overflow: "hidden",
    textOverflow: "ellipsis"
  },
  attendees: {
    color: "rgba(255, 255, 255, 0.7)"
  }
}));

export default function EventCard(props) {
  const classes = useStyles();

  return (
    <Card className={classes.root} variant="outlined">
      <CardActionArea href={`/callemall-meetup-app/events/${props.id}`}>
        <Typography className={classes.time}> {props.time}</Typography>
        <Typography className={classes.title} variant="h5">
          {props.name}
        </Typography>
        <Typography className={classes.location} varient="subtitle2">
          <Icon aria-label="LocationOn">
            <LocationOnIcon />
          </Icon>
          {props.venue}
        </Typography>
        <CardContent>
          <Typography nowrap className={classes.description}>
            {props.description}
          </Typography>
          <Typography className={classes.attendees}>
            {props.rsvp_count} attendees
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
