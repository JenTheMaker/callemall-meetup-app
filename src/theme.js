import { red } from "@material-ui/core/colors";
import { createMuiTheme } from "@material-ui/core/styles";

// A custom theme for this app, uses dark colors from React and MaterialUI's website.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#61dafb" // react logo blue
    },
    secondary: {
      main: "#fff"
    },
    error: {
      main: red.A400
    },
    background: {
      default: "#212121",
      header: "#333",
      card: "#424242"
    }
  }
});

export default theme;
