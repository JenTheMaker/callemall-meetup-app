// export default function fetchMeetupEventSpecific(eventID) {
//   const apiURL =
//     "https://api.meetup.com/reactjs-dallas/events/" +
//     eventID +
//     "?&sign=true&photo-host=public&fields=event_hosts";
//   return fetch(apiURL).then(res => res.json());
// }

export default function fetchMeetupEventSpecific(eventID) {
  const apiURL =
    "https://cors-anywhere.herokuapp.com/https://api.meetup.com/reactjs-dallas/events/" +
    eventID +
    "?&sign=true&photo-host=public&fields=event_hosts";
  return fetch(apiURL).then(res => res.json());
}

// Uncomment for JSON Server, TODO make it toggle with CLI
// export default function fetchMeetupEventRSVP(eventID) {
//   const apiURL = "http://localhost:8000/events/" + eventID;
//   return fetch(apiURL).then(res => res.json());
// }
