// export default function fetchMeetupEventRSVP(eventID) {
//   const apiURL =
//     "https://api.meetup.com/reactjs-dallas/events/" +
//     eventID +
//     "/rsvps?&sign=true&photo-host=public";
//   return fetch(apiURL).then(res => res.json());
// }

export default function fetchMeetupEventRSVP(eventID) {
  const apiURL =
    "https://cors-anywhere.herokuapp.com/https://api.meetup.com/reactjs-dallas/events/" +
    eventID +
    "/rsvps?&sign=true&photo-host=public";
  return fetch(apiURL).then(res => res.json());
}

// Uncomment for JSON Server, TODO make it toggle with CLI
// export default function fetchMeetupEventRSVP(eventID) {
//   const apiURL = "http://localhost:8000/" + eventID;
//   return fetch(apiURL).then(res => res.json());
// }
