// export default function fetchMeetupEvents() {
//   const apiURL =
//     "https://api.meetup.com/reactjs-dallas/events?&sign=true&photo-host=public&page=20";
//   return fetch(apiURL).then(res => res.json());
// }

export default function fetchMeetupEvents() {
  const apiURL =
    "https://cors-anywhere.herokuapp.com/https://api.meetup.com/reactjs-dallas/events?&sign=true&photo-host=public&page=20";
  return fetch(apiURL).then(res => res.json());
}

// Uncomment for JSON Server, TODO make it toggle with CLI
// export default function fetchMeetupEvents() {
//   const apiURL = "http://localhost:8000/events";
//   return fetch(apiURL).then(res => res.json());
// }
