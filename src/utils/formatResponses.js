import { values, mapValues, groupBy } from "lodash";

export function convertToReadableDate(epochDate) {
  // TODO make it a prettier
  return new Date(epochDate).toString();
}

export function convertDescriptionToText(description) {
  var temp = document.createElement("div");
  temp.innerHTML = description;
  return temp.textContent || temp.innerText || "";
}

export function convertRSVPList(eventRSVP) {
  let grouped = groupBy(eventRSVP, "response");
  let memberOnlyList = mapValues(grouped, list =>
    values(mapValues(list, "member"))
  );
  return memberOnlyList;
}

export default {
  convertToReadableDate,
  convertDescriptionToText,
  convertRSVPList
};
