// public
import React from "react";
import { Switch, Route } from "react-router-dom";

// private
import Events from "./Events";
import EventPage from "./EventPage";
import Home from "./Home";

// This handles all the route switching in the SPA

const Content = () => (
  <Switch>
    <Route exact path="/callemall-meetup-app" component={Home} />
    <Route exact path="/callemall-meetup-app/events" component={Events} />
    <Route path="/callemall-meetup-app/events/:eventID" component={EventPage} />
  </Switch>
);

export default Content;
