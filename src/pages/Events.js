// public
import React from "react";
import { Typography, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// private
import EventGrid from "../components/EventGrid";

const useStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.background.default,
    flex: "1 0 100%"
  },

  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(8),
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(16),
      paddingBottom: theme.spacing(16),
      flexDirection: "row",
      alignItems: "flex-start",
      textAlign: "left"
    }
  },
  title: {
    marginLeft: -12,
    whiteSpace: "nowrap",
    fontWeight: theme.typography.fontWeightBold,
    color: theme.palette.primary.main,
    paddingBottom: 8,
    [theme.breakpoints.only("xs")]: {
      fontSize: 28
    }
  }
}));

export default function Events() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container className={classes.content}>
        <Typography className={classes.title}> Upcoming Events</Typography>
        <EventGrid></EventGrid>
      </Container>
    </div>
  );
}
