// public
import React from "react";
import { Button, Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// private
import ReactJSDallasSVG from "../images/reactjs-dallas.svg";

const useStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.background.default,
    flex: "1 0 100%"
  },
  hero: {
    paddingTop: theme.spacing(8),
    color: theme.palette.primary.main
  },
  about: { color: theme.palette.secondary.main },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(8),
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(16),
      paddingBottom: theme.spacing(16),
      flexDirection: "row",
      alignItems: "flex-start",
      textAlign: "left"
    }
  },
  title: {
    marginLeft: -12,
    whiteSpace: "nowrap",
    letterSpacing: ".7rem",
    textIndent: ".7rem",
    fontWeight: theme.typography.fontWeightLight,
    color: theme.palette.primary.main,
    paddingBottom: 8,
    [theme.breakpoints.only("xs")]: {
      fontSize: 28
    }
  },
  logo: {
    flexShrink: 0,
    width: 120,
    height: 120,
    marginBottom: theme.spacing(2),
    [theme.breakpoints.up("md")]: {
      marginRight: theme.spacing(8),
      width: 195,
      height: 175
    },
    maxWidth: "inherit"
  },
  button: {
    marginTop: theme.spacing(4)
  }
}));

export default function HomePage() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <main id="main-content" tabIndex="-1">
        <div className={classes.hero}>
          <Container maxWidth="md" className={classes.content}>
            {/* TODO INSERT LOGO HERE */}
            <ReactJSDallasSVG className={classes.logo} />
            <div>
              <Typography variant="h3" component="h1" className={classes.title}>
                ReactJS Dallas
              </Typography>
              <Typography className={classes.about} variant="h5" component="h2">
                We love Facebook's ReactJS, open-source projects, and developing
                apps for the web. Whether you are a seasoned veteran or a
                newcomer to React, join us to learn more about why React is
                gaining traction among developers around the world. Come for the
                free food and drink, stay for the great conversation and
                learning!
              </Typography>
              <Button
                href="/callemall-meetup-app/events"
                className={classes.button}
                variant="outlined"
                color="secondary"
              >
                Upcoming Events
              </Button>
            </div>
          </Container>
        </div>
      </main>
    </div>
  );
}
