// public
import React from "react";
import {
  Box,
  Container,
  GridList,
  GridListTile,
  Typography
} from "@material-ui/core";

// private
import fetchMeetupEventSpecific from "../api/FetchMeetupEventSpecific";
import fetchMeetupEventRSVP from "../api/FetchMeetupEventRSVP";
import {
  convertToReadableDate,
  convertRSVPList
} from "../utils/formatResponses";
import EventHeader from "../components/EventHeader";
import EventAvatars from "../components/EventAvatars";

export default class EventPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      eventRSVP: [],
      eventID: null
    };
  }

  componentDidMount() {
    const {
      match: { params }
    } = this.props;

    Promise.all([
      fetchMeetupEventSpecific(params.eventID),
      fetchMeetupEventRSVP(params.eventID)
    ])
      .then(([res1, res2]) => {
        this.setState({
          isLoaded: true,
          event: res1,
          eventRSVP: Object.values(res2),
          eventID: params.eventID
        });
      })
      .catch(error => {
        this.setState({
          isLoaded: true,
          error
        });
      });
  }

  render() {
    const { error, isLoaded, event, eventRSVP } = this.state;
    if (error) {
      return (
        <Container className="container">Error: {error.message}</Container>
      );
    } else if (!isLoaded) {
      return <Container className="container">Loading...</Container>;
    } else {
      let rsvpList = convertRSVPList(eventRSVP);
      let yesList = rsvpList.yes;
      // TODO add wait and no list
      return (
        <Container className="container">
          <Box>
            <EventHeader
              key={event.id}
              id={event.id}
              name={event.name}
              time={convertToReadableDate(event.time)}
              venue={event.venue.name}
              rsvp_count={event.yes_rsvp_count}
              description={event.description}
            ></EventHeader>
            <Typography color="secondary">Hosted By: </Typography>
            {event.event_hosts.map(hosts => (
              <EventAvatars
                key={hosts.id}
                id={hosts.id}
                name={hosts.name}
                img={hosts.photo.photo_link}
              />
            ))}
          </Box>
          <Container>
            <Typography color="secondary">
              Attendees ({yesList.length})
            </Typography>
            <GridList cols={8}>
              {yesList.map(member => (
                <GridListTile>
                  <EventAvatars
                    key={member.id}
                    id={member.id}
                    name={member.name}
                    img={"photo" in member ? member.photo.photo_link : null}
                  />
                </GridListTile>
              ))}
            </GridList>
          </Container>
        </Container>
      );
    }
  }

  componentWillUnmount() {}
}
