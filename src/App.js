// public
import React from "react";
import "typeface-roboto";

// private
import Content from "./pages/Content";
import NavBar from "./components/NavBar";

function App() {
  return (
    <React.Fragment>
      <NavBar key="navigation"></NavBar>
      <Content key="content"></Content>
    </React.Fragment>
  );
}

export default App;
