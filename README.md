**Coding Challenge Instructions:**

**Build and deploy a React application that meets the following criteria:**

- Consume Meetup.com web API and display information about an upcoming ReactJS Dallas event
  - Use the v3 events endpoint to get event information (the urlname for the group is “reactjs-dallas”):
    - Docs: https://secure.meetup.com/meetup_api/console/?path=/:urlname/events
    - Url: https://api.meetup.com/reactjs-dallas/events?&sign=true&photo-host=public&page=1
  - Use the v3 rsvps endpoint to get rsvps for an event. Use the “id” of the event returned from the call above.
    - Docs: https://secure.meetup.com/meetup_api/console/?path=%2F%3Aurlname%2Fevents%2F%3Aevent_id%2Frsvps
    - Url: https://api.meetup.com/reactjs-dallas/events/{event-id}/rsvps?&sign=true&photo-host=public
- Use more than one react component
- Use both state and props
- Demonstrate knowledge of component lifecycle

**Please include the following deliverables in your submission:**

- Share all source code for your application on GitHub along with commit history
- Provide a url for us to view a live version of your application

---

### Time it had taken to develop

Meeting the initial requirements to connect to the API with React.js took a few hours to setup and deploy live.

The bulk of the extra few days I spent on familiarizing myself with Material UI and various aspects of React. I learned a lot on this project on best ways to route and handle react component styling. If I were to do it again, I think it would have been nice to incorporate Redux in order to better share state and allow for a more seamless experience. I would have also liked to have spent more time styling.

### Thoughts behind the Look&Feel

The inspiration behind this application was a mixture of Meetup.com, MaterialUi.com, and ReactJSDallas.

I went with a dark pallete in the theme, with touches of React Blue. I tried to follow the general design of MeetUp.com's event and group pages.


### Future Plans

Things I would have liked to have added would include:

- Add testing and better cross-device support
- Use redux to pass state, in order to reduce API call to retrieve single event information
- Use OAuth to handle CORS issue, and not need to redirect
- Better handling of loading screens and animations
- Home Page: added links and buttons for all social media sites
- Events Page: Add attend button that links to meetups.com
- Event Specific Page: Overall better Look&Feel, more information displayed as well
- There are also TODO comments within the code for other changes I think would help improve this application

### Tools + Libraries Used

Create React App
MaterialUI
React Router
JSON-Server
Prettier
VSCode

SVG Tools:
https://jakearchibald.github.io/svgomg/

### Setup

`yarn install`

To start locally and connected with CORS to Meetup API

`yarn start`

To start mock JSON Server with a few test values

`yarn start-jserver`

#### To run in production

The project automatically deploys to https://jenthemaker.gitlab.io/callemall-meetup-app/
using GitLab's CICD runner.

#### Things to note:

Local port is 3000
JSON Server is on 8000

### Project Structure

    .
    ├── json_server         # Contains mock data to test locally without connecting to API
    ├── public
    ├── src
    │   ├── api             # Functions to fetch API from api.meetup.com
    │   ├── components      # Stylized and custom components built from MaterialUI
    │   ├── images          # Custom SVGIcons used for MaterialUI
    │   ├── pages           # Main Pages and routeing
    │   ├── util            # Utility files to use for formating JSON response
    │   ├── App.js
    │   ├── theme.js        # Dark theme colors
    │   └── index.js
    ├── .gitlab-ci.yml      # CICD for Gitlab to auto deploy with master, caches for speed
    ├── todo.txt            # TODO list for future reference
    ├── LICENSE
    └── README.md

